import time
import sys
from flask import Flask, render_template, url_for, redirect
from re import M

sys.path.append('./src/')
from cli import *
from musicList import GeneralMusicList, HappyMusicList, SadMusicList

app = Flask(__name__)
MusicList = []


emotionVAl=val()
# emotionVAl = [1, 1, 17, 1, 0]#test case


indexMax = emotionVAl.index(max(emotionVAl)) #gets which emotion is the highest
if(indexMax == 2):
    MusicList = SadMusicList
elif(indexMax == 3):
    MusicList = HappyMusicList
else:
    MusicList = GeneralMusicList


@app.route('/')  # set the home path
def upload_file():
    return render_template("index.html", log=emotionVAl, musics=MusicList)

progressMeter = "[#######"
completionMeter = "[###################################################"  # 51

for i in range(100):
    if (i == 1 or i == 10 or i == 30):
        time.sleep(0.3)
    print('\r'+progressMeter, end='')
    time.sleep(0.15)
    if(i == 25):
        time.sleep(1.5)

    progressMeter += '#'
    if progressMeter == completionMeter:
        break

print(']')

app.run(host='0.0.0.0', port=6969)
