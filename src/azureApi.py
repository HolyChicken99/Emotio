import requests
import json
from flask import jsonify


def image_response(path):
    key = 'b7172f58e43c491ea674dcdd81865e80' 
    url = "https://holychicken99.cognitiveservices.azure.com/face/v1.0/detect"
    params = {"returnFaceAttributes": "emotion"}
    headers = {
        'content-type': "application/octet-stream", # octet-stream from binary file
        'Ocp-Apim-Subscription-Key': "b7172f58e43c491ea674dcdd81865e80"} # auth key for azure services

    path_image = path
    with open(path_image, 'rb') as f:
        data = f.read() # converts the image to binary format 
    response = requests.request(
        "POST", url, data=data, headers=headers, params=params)
    data = response.json()
    # print (data)
    return data

                                                                                                                                        


if __name__ == '__main__':
    image_response('pewds.jpg')
