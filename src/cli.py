import argparse
import os
import sys
from face_api import *
from emotion_value import *
from video_verify import *
import glob
import enquiries


def checkImage(path):  # function to check if the opened file is valid or not
    if path == '':
        return
    ext = os.path.splitext(path)
    allowedExtensionsImage = ['.jpg', 'png', '.jpeg', '.mp4', '.wav']

    # checks the extensions
    if not ext[1] in allowedExtensionsImage:
        print("\033[36m wrong or no file found r\n")
        raise Exception(
            " \033[44m \033[01m   incorrect extension given " + ext[1])


os.system(" cat ./static/assets/logo.txt | lolcat")  # outputs the magnus logo
sys.stdout.flush()

# adds the command line arguments to the executable
parser = argparse.ArgumentParser(
    prog='PROG', usage='%(prog)s [options]',
    description='\033[32m Program to find the music playlist based on the highest scoring emotion in the passed image/video visit docx for more info:   https://google.com ')

parser.add_argument(
    '--cli', help='\033[31m  output data to the command prompt only')
args = parser.parse_args()

# provides the options
path = ''
options = ['Path to saved Image', 'webcam-feed']
choice = enquiries.choose('Choose one of these options: ', options)
if enquiries.confirm('continue with the selected option ?'):
    pass

print(choice)

# inputs the path to the static image
if choice == "Path to saved Image":
    path = input("enter the path ...... r\n  ->")

# check the path to image
checkImage(path)


def val():
    return emotionArr(emomtionRet(path))
