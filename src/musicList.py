
class Music:
    def __init__(self,name,url):
        self.name = name
        self.url = url

Happy1=Music("Pharrell Williams - Happy","https://www.youtube.com/watch?v=y6Sxv-sUYtM")
Happy2=Music("Jason Derulo - Kiss The Sky" ,"https://www.youtube.com/watch?v=APyyMrH7HFY")
Happy3=Music("Justin Timberlake -  CAN'T STOP THE FEELING!", "https://www.youtube.com/watch?v=ru0K8uYEZWw")
HappyMusicList=[Happy1,Happy2,Happy3]
################################################################
Sad1=Music("OZZIE - FRZZN (feat. Teflon Sega)","https://www.youtube.com/watch?v=RIrDKNc6sMs")
Sad2=Music("Cats In The Cradle-Harry Chapin","https://www.youtube.com/watch?v=KUwjNBjqR-c")
Sad3=Music("Shigatsu wa Kimi no Uso 1","https://www.youtube.com/watch?v=5_iuNaULT5k")
SadMusicList=[Sad1,Sad2,Sad3]
#########################
general1=Music("INXS - Disappear","https://www.youtube.com/watch?v=Xvo-bfaVsg0")
general2=Music("John Newman - Love Me Again","https://www.youtube.com/watch?v=CfihYWRWRTQ")
general3=Music("Fox Stevenson - Like That","https://www.youtube.com/watch?v=4iU09oK_pI4")
GeneralMusicList=[general1,general2,general3]
